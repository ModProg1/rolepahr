#!/bin/bash
name=$(hostname --short);
mkdir $name
echo $name >>"$name/timing.test"
echo $name >>"$name/output.test"
for ((var=1; var <= 12; var++))
do
    echo testing $var 
    echo -n testing $var >>"$name/timing.test"
    { time ../partdiff-posix $var 2 512 2 2 600 >>"$name/output.test";} 2>>"$name/timing.test"
    echo  >>"$name/timing.test"
done