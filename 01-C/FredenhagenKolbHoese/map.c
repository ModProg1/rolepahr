#include <stdio.h>

// Definieren Sie ein enum cardd
enum cardd
{
	//Schiebt 2 nach links um die Binärbelegung für die
	// Binärenoder und Binärund Operationen zu bekommen.
	N = 1 << 0, //1
	S = 1 << 1, //10
	W = 1 << 2, //100
	E = 1 << 3  //1000
};
// Definieren Sie ein 3x3-Array namens map, das Werte vom Typ cardd enthält

enum cardd map[3][3];

// Die Funktion set_dir soll an Position x, y den Wert dir in das Array map eintragen
// Überprüfen Sie x und y um mögliche Arrayüberläufe zu verhindern
// Überprüfen Sie außerdem dir auf Gültigkeit
void set_dir(int x, int y, enum cardd dir)
{
	//Überprüfe 0 <= xy < 3 (Länge des Arrays)
	if (x < 3 && x >= 0 && y < 3 && y >= 0)
		//Überprüfe dir, so dass nicht Nord und Süd bzw. Ost und West gleichzeitig "wahr" sein können
		if (!(dir & N && dir & S || dir & E && dir & W))
			map[y][x] = dir;
}

// Die Funktion show_map soll das Array in Form einer 3x3-Matrix ausgeben
void show_map(void)
{
	for (int y = 0; y < 3; y++)
	{
		for (int x = 0; x < 3; x++)
		{
			//two ist 1, wenn sowohl Nord oder Süd, sowie Ost oder West "wahr" ist
			//d.h. two==1 bei N|E und two==0 bei N
			int two = map[x][y] & (N | S) && map[x][y] & (W | E);
			//setzt die Abstandshalter vor der letzten Stelle, wenn nur eine Himmelsrichtung
			//nur einen, und bei zwei zwei
			if (x == 2)
				printf(two ? "_" : "__");
			switch (map[x][y])
			{
			case N:
				printf("N");
				break;
			case N | E:
				printf("NE");
				break;
			case E:
				printf("E");
				break;
			case S | E:
				printf("SE");
				break;
			case S:
				printf("S");
				break;
			case S | W:
				printf("SW");
				break;
			case W:
				printf("W");
				break;
			case N | W:
				printf("NW");
				break;
			default:
				printf("0");
				break;
			}
			//Setzt den Abstandhalter nach der 2. Himmelsrichtung
			// nur bei ein langen Himmelsrichtungen, da ein Abstandshalter bereits davor gesetzt wird, s.u.
			if (x == 1 && !two)
				printf("_");
			//Setzt den Abstandhalter nach der 1. und vor der 2. Himmelsrichtung, deshalb 2 und 3 statt 1 und 2
			else if (x == 0)
				if (two)
					printf("__");
				else
					printf("___");
		}
		printf("\n");
	}
}

int main(void)
{
	// In dieser Funktion darf nichts verändert werden!
	set_dir(0, 1, N);
	set_dir(1, 0, W);
	set_dir(1, 4, W);
	set_dir(1, 2, E);
	set_dir(2, 1, S);

	show_map();

	set_dir(0, 0, N | W);
	set_dir(0, 2, N | E);
	set_dir(0, 2, N | S);
	set_dir(2, 0, S | W);
	set_dir(2, 2, S | E);
	set_dir(2, 2, E | W);
	set_dir(1, 3, N | S | E);
	set_dir(1, 1, N | S | E | W);

	show_map();

	return 0;
}
