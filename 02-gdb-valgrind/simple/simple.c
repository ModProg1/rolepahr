/*
** simple error demonstration to demonstrate power of valgrind
** Julian M. Kunkel - 17.04.2008
*/

#include <stdio.h>
#include <stdlib.h>

int *mistake1()
{
  // sichert buf speicher, damit der Inhalt auch in main()
  // genutzt werden kann
  int *buf = (int *)malloc(sizeof(int) * 6);
  int temp[] = {1, 1, 2, 3, 4, 5};
  // anschließend werde alle Ellemente die in buf
  // sein sollen aus temp in buf gepackt
  for (int i = 0; i < 6; i++)
  {
    buf[i] = temp[i];
  }

  return buf;
}

int *mistake2()
{
  int *buf = (int *)malloc(sizeof(int) * 4);
  buf[1] = 2;
  return buf;
}

int *mistake3()
{
  /* In dieser Funktion darf kein Speicher direkt allokiert werden. */
  // nicht benötigt
  // int mistake2_ = 0;
  // Holt sich allokierten speicher von mistake2
  int *buf = mistake2();
  buf[0] = 3;
  return buf;
}

int *mistake4()
{
  // Allokiert Platz für einen int
  int *buf = (int *)malloc(sizeof(int));
  //buf wird als int Pointer nicht als Array verwendet
  *buf = 4;
  //darf nicht gefreed werden, sonnst geht der wert verloren
  //free(buf);
  return buf;
}

int main(void)
{
  /* Modifizieren Sie die folgende Zeile nicht */
  int *p[4] = {&mistake1()[1], &mistake2()[1], mistake3(), mistake4()};

  printf("1 %d\n", *p[0]);
  printf("2 %d\n", *p[1]);
  printf("3 %d\n", *p[2]);
  printf("4 %d\n", *p[3]);

  /* mhh muss hier noch etwas gefreed werden? */
  /* Fügen sie hier die korrekten aufrufe von free() ein */
  free(p[0] - 1); /* welcher Pointer war das doch gleich?, TODO: Fixme... :-) */
  free(p[1] - 1);
  free(p[2]);
  free(p[3]);
  return 0;
}
