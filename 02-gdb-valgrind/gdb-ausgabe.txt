Führe im Verzeichnis simple make aus:
Liefert 2 Warnungen:
```
gcc -ggdb -Wall simple.c -o simple
simple.c: In Funktion »mistake1«:
simple.c:13:10: Warnung: Funktion liefert Adresse einer lokalen Variablen zurück [-Wreturn-local-addr]
13 |   return buf;
    |          ^~~
simple.c: In Funktion »mistake3«:
simple.c:28:7: Warnung: Variable »mistake2_« wird nicht verwendet [-Wunused-variable]
28 |   int mistake2_ = 0;
    |       ^~~~~~~~~
```
Starten von gdb mit `gdb ./simple` liest simple ein `Reading symbols from ./simple...`
Anschließend kann man  mit `b` einen *Breakpoint* setzen und das Program mit `run` starten:
```
(gdb) b 12
Breakpoint 1 at 0x1180: file simple.c, line 12.
(gdb) run
Starting program: /drives/data/Uni/HR/rolepahr/02-gdb-valgrind/simple/simple 

Breakpoint 1, mistake1 () at simple.c:12
12        int buf[] = { 1, 1, 2, 3, 4, 5 };
```

Wenn man sich jetzt die Werte für `buf` und `buf[2]` ausgibt, bekommt man zwar einen Integerarray, aber noch nicht mit sinnvollen Werden, da der Array an der Stelle von `buf` noch nicht initialisiert wurde. Wiederholt man das in der nächsten Zeile bekommt man die erwarteten Werte. `buf` ist hier ein *Integerarray*:
```
(gdb) print buf
$1 = {194, 0, -9161, 32767, -9162, 32767}
(gdb) print buf[2]
$2 = -9161

(gdb) n
13        return buf;

(gdb) print buf
$3 = {1, 1, 2, 3, 4, 5}
(gdb) print buf[2]
$4 = 2
```

Nach dem setzen des Breakpoints bei `mistake2` kann das Programm mit `c continue` fortgesetzt werden:
```
(gdb) b mistake2
Breakpoint 2 at 0x5555555551cd: file simple.c, line 19.
(gdb) c
Continuing.

Breakpoint 2, mistake2 () at simple.c:19
19        int *buf = malloc (sizeof (char) * 4);
```
`buf` ist hier ein *Pointer*:
```
(gdb) print buf
$4 = (int *) 0xf9edd4f601852300
```
Setzt man das Programm jetzt fort, bekommt man eine Fehlermeldung:
```
(gdb) c
Continuing.

Program received signal SIGSEGV, Segmentation fault.
0x0000555555555209 in mistake3 () at simple.c:30
30        buf[0] = 3;
(gdb) list
25      mistake3 ()
26      {
27        /* In dieser Funktion darf kein Speicher direkt allokiert werden. */
28        int mistake2_ = 0;
29        int *buf = (int *) &mistake2;
30        buf[0] = 3;
31        return buf;
32      }
33
```
Die *Frames* auf dem *Stack* kann man sich mit `bt backtrace` ausgeben. Wenn man mit `frame 1` zur `main()` Funktion wechselt, kann man sich `p` herausgeben lassen mit `pint p`:

```
(gdb) bt
#0  0x0000555555555209 in mistake3 () at simple.c:30
#1  0x0000555555555290 in main () at simple.c:47
(gdb) frame 1
#1  0x0000555555555290 in main () at simple.c:47
47        int *p[4] = { &mistake1 ()[1], &mistake2 ()[1], mistake3 (), mistake4 () };
(gdb) print p
$5 = {0x4, 0x5555555592a4, 0x555555555330 <__libc_csu_init>, 0x555555555070 <_start>}
```

`mistake4()` gibt einen Pointer zurück:
```
(gdb) call mistake4()
$7 = (int *) 0x5555555592c0
```