#Aufgabe 3
##1.
###Erster Durchlauf mit unveränderten Makefile:
time ./partdiff-seq 1 2 64 1 2 10240

Berechnungszeit:    131.527779 s
Berechnungsmethode: Jacobi
Interlines:         64
Stoerfunktion:      f(x,y)=0
Terminierung:       Anzahl der Iterationen
Anzahl Iterationen: 10240
Norm des Fehlers:   2.753424e-05
Matrix:
 1.0000 0.8750 0.7500 0.6250 0.5000 0.3750 0.2500 0.1250 0.0000
 0.8750 0.5042 0.3082 0.2306 0.1820 0.1372 0.0996 0.0909 0.1250
 0.7500 0.3082 0.0991 0.0477 0.0349 0.0276 0.0346 0.0996 0.2500
 0.6250 0.2306 0.0477 0.0080 0.0035 0.0048 0.0276 0.1372 0.3750
 0.5000 0.1820 0.0349 0.0035 0.0006 0.0035 0.0349 0.1820 0.5000
 0.3750 0.1372 0.0276 0.0048 0.0035 0.0080 0.0477 0.2306 0.6250
 0.2500 0.0996 0.0346 0.0276 0.0349 0.0477 0.0991 0.3082 0.7500
 0.1250 0.0909 0.0996 0.1372 0.1820 0.2306 0.3082 0.5042 0.8750
 0.0000 0.1250 0.2500 0.3750 0.5000 0.6250 0.7500 0.8750 1.0000

real    2m11.541s
user    2m11.531s

time ./partdiff-seq 1 2 64 2 2  5120

real    2m6.501s
user    2m6.461s
sys     0m0.020s

#Makefile mit O3
time ./partdiff-seq 1 2 64 1 2 10240

Berechnungszeit:    106.988073 s
real    1m47.003s
user    1m46.954s
sys     0m0.020s


#Makefile mit O2
time ./partdiff-seq 1 2 64 1 2 10240

Berechnungszeit:    98.809647 s				##Beste Laufzeit
real    1m38.832s
user    1m38.806s
sys     0m0.008s

#Makefile mit O1
time ./partdiff-seq 1 2 64 1 2 10240

Berechnungszeit:    115.491230 s
real    1m55.507s
user    1m55.387s
sys     0m0.020s

#gcc mit 0fast
time ./partdiff-seq 1 2 64 1 2 10240
Berechnungszeit:    50.418257 s
real    0m50.428s
user    0m50.407s
sys     0m0.004s

time ./partdiff-seq 1 2 64 2 2  5120
Berechnungszeit:    96.395129 s
real    1m36.403s
user    1m36.359s
sys     0m0.004s

##2.
Analy
gprof ./partdiff-seq

  %   cumulative   self              self     total
 time   seconds   seconds    calls   s/call   s/call  name
 95.39    141.29   141.29        1   141.29   147.72  calculate
  4.34    147.72     6.43 2758256640     0.00     0.00  getResiduum
  0.33    148.22     0.50        1     0.50     0.50  initMatrices
  0.00    148.22     0.00        4     0.00     0.00  allocateMemory
  0.00    148.22     0.00        1     0.00     0.00  AskParams
  0.00    148.22     0.00        1     0.00     0.00  DisplayMatrix
  0.00    148.22     0.00        1     0.00     0.00  allocateMatrices
  0.00    148.22     0.00        1     0.00     0.00  displayStatistics
  0.00    148.22     0.00        1     0.00     0.00  freeMatrices
  0.00    148.22     0.00        1     0.00     0.00  initVariables

Die Funktion calculate wird ein mal aufgerufen,
braucht aber 95% der Zeit um ausgeführt zu werden.
=> Hauptfunktion die optimiert werden muss

getResiduum ist 2758256640 aufgerufen worden,
nutz aber nur 4% der Rechenzeit
=> vielleicht auch möglich zu optimieren

##3.

Optimierung durch Vertauschung der Schleifen.
Zeile 228 und Zeile 231.
Der Cache wird anders Aufgerufen und ermöglicht somit schnellere Zugriffs Zeit.

Ausführung ohne GCC Optimierungen für:
time ./partdiff-seq 1 2 64 1 2 10240
Berechnungszeit:    89.600633 s
real    1m29.613s
user    1m29.587s
sys     0m0.000s


Mit GCC Optimierung -Ofast:
time ./partdiff-seq 1 2 64 1 2 10240
Berechnungszeit:    6.185814 s
real    0m6.195s
user    0m6.181s
sys     0m0.008s

time ./partdiff-seq 1 2 64 2 2  5120
Berechnungszeit:    19.594749 s

real    0m19.605s
user    0m19.591s
sys     0m0.004s

Nach Optimierung durch den Compiler, brachten Umschreiben der star Formel in calculate
o.Ä. keine messbaren Performanceunterschiede.

##4.
Performance counter stats for './partdiff-seq 1 2 64 1 2 10240':

      89573.223183      task-clock (msec)         #    0.999 CPUs utilized          
             7,492      context-switches          #    0.084 K/sec                  
                 0      cpu-migrations            #    0.000 K/sec                  
             1,128      page-faults               #    0.013 K/sec                  
   238,412,218,256      cycles                    #    2.662 GHz                      (83.35%)
   104,688,403,334      stalled-cycles-frontend   #   43.91% frontend cycles idle     (83.33%)
     8,925,910,695      stalled-cycles-backend    #    3.74% backend cycles idle      (66.67%)
   480,239,565,869      instructions              #    2.01  insn per cycle         
                                                  #    0.22  stalled cycles per insn  (83.34%)
    22,102,060,643      branches                  #  246.749 M/sec                    (83.33%)
         9,138,435      branch-misses             #    0.04% of all branches          (83.33%)

      89.642607822 seconds time elapsed

  perf stat zählt bestimmte Events wie context-switches, cycles und auch stalled-cycles. Wobei stalled-cycles die relevanten sind, da diese die CPU blockieren.