﻿### Aufgabe 1:
1. Batch Queuing?
   - Nichtinteraktive Datenverarbeitung
   - Aufgaben werden als `Batch` ans System Übergeben
   - Führt zu guter Auslastung der Ressourcen
   - Vereinfacht Mehrbenutzerbetrieb
2. Batch Queuing System?
   - Stellt Batch Queuing zur Verfügung
   - Führt mit Beachtung von Parametern wie
     - Priorität
     - erwartete Laufzeit
     - Ressourcen Verbrauch
    übergebene Aufgaben aus.
3. Beispiele für Batch Queuing Systeme
   1. Maui Cluster Scheduler
   2. OpenPBS oder TORQUE
   3. OpenLava
4. System des DKRZ
   - Simple Linux Utility for Resource Management
5. `sbatch`
   - Übergibt ein Batch-Skript an Slurm.
     - Als Datei oder Input-Stream
   - Bei Ausführen werden Ausgaben und Fehlermeldungen in eine Datei geschrieben
   - Wenn Ausführung möglich, wird das Script auf dem ersten Verfügbaren Knoten ausgeführt.
6. Jobs und Status?
   - `scontrol` 
7. `sview`
   - bietet graphische Oberfläche
   - lässt sich leichter ohne studieren der man pages bedienen
   - Bessere Darstellung von Listen
8. Stoppen und Löschen von Jobs
   - `scancel <JobID>`
   - Jobs können direkt terminiert werden
   - Mit `-s` können auch Signale direkt an das Programm gesendet werden
9. Mehrfachbenutzung von Knoten?
   - Ja, abhängig von den Anforderungen der Jobs
10. Detaillierte Informationen zu Jobs
   - Mit `sstat` lassen sich Details zu Jobs anzeigen.
11. Mögliche Scheduling-Verfahren
    1.  Backfill
        - führt Jobs, die zwar nicht die höchste Priorität haben aus,
          wenn da durch die Startzeit höherer Jobs nicht beeinträchtigt wird.
        - am DKRZ verwendet
    2.  nur Priorität wird beachtet
12. `hostname` auf `west7` ausführen:
    -  `echo -e '#!/bin/sh\nhostname' | sbatch -w west7 --partition=west -ohostname.out` 
    -  Anschließend steht in der Datei `hostname.out` `west7`, der Name des ausführenden      Knotens.
13. Über den Befehl:
    - `scontrol show config`, wird bei `SlurmdTimeout` und `SlurmctldTimeout`, 300sec angezeigt.

14. Priorität des Jobs nachschauen:
    - `scontrol show job <JobID> | grep Priority`
	Priorität ändern:
	- `scontrol update NodeName=<Name> Priority=<value>`
	geht aber nur mit ausreichenden Berechtigungen.
15. Der Befehl zum Anzeigen:
    - `scontrol show partitions | grep Name`
	Die Partitionen auf dem Cluster sind:
	- abu
	- amd
	- magny
	- nehalem
	- west
	Änderung der Partitionen:
	- `scontrole hide partitions`
	Versteckt Partitionen
	- `scontrole delete Name=<name>`
	Löscht Partitionen
### Aufgabe 2:
4. 
   1. Was fällt Ihnen auf?
       - Bei mehrfacher Ausführung werden die Dateien `timescript.out` Überschieben.
       - Die Reihenfolge der Prozessausführung ist immer die selbe, (west9, west10, west9, west9, west9, west10, west10, west10).*
       - Die Reihenfolge der Rückgabe, spiegelt nicht die Reihenfolge der Ausführung wieder:
       ```bash
       west9:  2019-11-01T01:13:31,877364022+01:00
       west10: 2019-11-01T01:13:31,880359092+01:00 # 2. auf 10, nach timestamp
       west9:  2019-11-01T01:13:31,877388999+01:00
       west9:  2019-11-01T01:13:31,877496116+01:00
       west9:  2019-11-01T01:13:31,877533930+01:00
       west10: 2019-11-01T01:13:31,880356597+01:00 # 1. auf 10
       west10: 2019-11-01T01:13:31,881509502+01:00 # 4. auf 10
       west10: 2019-11-01T01:13:31,881413096+01:00 # 3. auf 10
       ```
       - Abwechslung der Kerne* 
         - => Vermutung: Ausgleich der Rechenlast für z.B. bessere Kühlung?
       * Bemerkung: *Test nur auf 2 von 4 Knoten, weil die anderen Konten zu unserer Zeit nicht erreichbar waren.*
   2. Könnte man die Datei `timescript.out` auch innerhalb des Skriptes `timescript` erzeugen?
      - ja
      - Durch ändern von `timescript`:
       ```bash
        # Erzeugt timescript.out wenn noch nicht vorhanden
        touch timescript.out
        # >> fügt die Ausgabe von dem Befehl davor, hier echo an timescript.out an
        echo "$(hostname --short): $(date --iso-8601=ns)" >> timescript.out
       ```
